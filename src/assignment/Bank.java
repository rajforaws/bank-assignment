package assignment;

public class Bank {

	
	String Owner;
	String City;
	String State;
	String pinCode;
	String AccountType;
	int Balance;
	String DateOfCreation;
	String Status;
	
	public Bank(String owner, String city, String state, String pinCode,String accType, int balance, String dateOfCreation , String status) {
		
		Owner = owner;
		City = city;
		State = state;
		this.pinCode = pinCode;
		this.AccountType = accType;
		Balance = balance;
		DateOfCreation = dateOfCreation;
		Status = status;
	}
	@Override
	public String toString() {
		return "Bank [Owner=" + Owner + ", City=" + City + ", State=" + State + ", pinCode=" + pinCode + ", Balance="
				+ Balance + ", DateOfCreation=" + DateOfCreation + "]";
	}

	
	public void addBalance(int deposit) {
		System.out.println("Balance: " + Balance);
		this.Balance += deposit;
		System.out.println("New Balance: " + Balance);
		System.out.println("-------------------------------------------------------------");
	}
	
	public void withdraw( int amount){
		System.out.println("Balance: " + Balance);
		
		if( amount > Balance) {
			System.out.println("Sorry ! request cant be accccepted due to insufficient balance");
		}
		else {
			this.Balance -= amount;
			System.out.println("New Balance: " + Balance);
		}
		System.out.println("-------------------------------------------------------------");
	}
}
enum Status{
	ACTIVE,
	INACTIVE,
	CLOSED;
}

enum AccountType{
	
	SAVINGS,
	CuRRENT;
}